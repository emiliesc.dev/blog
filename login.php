<?php
require ('config.php');
if (isset($_POST['pseudo']) && isset($_POST['mdp'])) {
    $pseudo = strip_tags($_POST['pseudo']) ?? '';
    $pass = strip_tags($_POST['mdp']) ?? '';
    $req = $db->prepare('SELECT * FROM utilisateurs WHERE pseudo = :pseudo AND mot_de_passe = :pass');
    $req->execute(array(
        ':pseudo' => $pseudo,
        ':pass' => $pass));
    if ($req->rowCount() === 1) {
        $resultat = $req->fetch();
        if (!$resultat) {
            echo '<div class="error-login">Vos identifiants sont incorrects !</div>';
        } else {
            header('Location:../index.php');
            session_start();
            $_SESSION['pseudo'] = $resultat['pseudo'];
            $_SESSION['mdp'] = $pass;
        }
    }
}

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/skins/purple.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <title>Espace connexion</title>
</head>
<body>
<main>
    <a href="index.php"><img id="logo" src="images/logo.png" alt="mister diner, logo"></a>
    <div id="formulaire_co">
        <form action="accueil.php" method="post">
            <h1>Connexion</h1>
            <div class="mb-3">
                <label for="pseudo" class="form-label">Pseudo :</label><br>
                <input type="text" class="form-control" name="pseudo" id="pseudo">
            </div>
            <div class="mb-3">
                <label for="mdp" class="form-label">Mot de passe :</label><br>
                <input type="password" name="mdp" class="form-control" id="mdp">
            </div>
            <div id="bouton_connexion">
                <input type="submit" value="Connexion">
            </div>
            <div id="bouton_mdp">
                <a href="#">Mot de passe oublié</a><br>
            </div>
        </form>
    </div>
</main>
</body>
</html>